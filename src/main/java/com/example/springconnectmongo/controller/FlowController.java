package com.example.springconnectmongo.controller;

import com.example.springconnectmongo.dto.flow.Flow;
import com.example.springconnectmongo.service.FlowService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@Slf4j
@RestController
@RequestMapping("/api/flow")
public class FlowController {

    @Autowired
    private FlowService flowService;

    @PostMapping()
    public ResponseEntity<?> addFlow(@RequestBody Flow body) {
        Flow flow = flowService.create(body);
        return ResponseEntity.status(HttpStatus.CREATED).body(flow);
    }

    @PutMapping("/{id}")
    public ResponseEntity<?> putCustomers(@PathVariable String id, @RequestBody Flow body) {
        Optional<?> flow = flowService.update(id, body);
        return ResponseEntity.ok(flow);
    }

    @GetMapping()
    public ResponseEntity<?> getAllFlow() {
        List<Flow> flow = flowService.getAll();
        return ResponseEntity.ok(flow);
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> getFlowById(@PathVariable String id) {
        Optional<Flow> flow = flowService.getById(id);
        if (!flow.isPresent()) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(flow);
    }
}
