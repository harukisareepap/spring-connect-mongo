package com.example.springconnectmongo.controller;

import com.example.springconnectmongo.entity.TblMdDataSourceEntity;
import com.example.springconnectmongo.service.DataSourceService;
import java.util.List;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@RequestMapping("/api")
public class DataSourceController {

  private final DataSourceService dataSourceService;

  @Autowired
  public DataSourceController(DataSourceService dataSourceService) {
    this.dataSourceService = dataSourceService;
  }

  @GetMapping("/dataSource")
  public ResponseEntity<?> findAllDataSource() {
    List<TblMdDataSourceEntity> dataSourceEntities = dataSourceService.findAllDataSource();
    return ResponseEntity.ok(dataSourceEntities);
  }

  @PostMapping("/data-source")
  public ResponseEntity<?> saveDataSource(@RequestBody TblMdDataSourceEntity entity) {
    dataSourceService.saveDataSource(entity);
    return ResponseEntity.status(HttpStatus.CREATED).body(entity);
  }
}
