package com.example.springconnectmongo.dto.flow;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.validation.constraints.NotNull;

@Data
public class Edge {
    @Id
    private String id;
    private String source;
    private String sourceHandle;
    private String target;
    private String targetHandle;
    private Boolean animated;
    @NotNull
    private String type;
    private String arrowHeadType;
}
