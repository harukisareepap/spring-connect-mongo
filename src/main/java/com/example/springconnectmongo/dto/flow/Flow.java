package com.example.springconnectmongo.dto.flow;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

@Data
@Document("TBL_RD_FLOW_INF")
public class Flow {
    @Id
    private String id;
    private List<Node> node;
    private List<Edge> edge;
}
