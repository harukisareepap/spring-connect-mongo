package com.example.springconnectmongo.dto.flow;

import lombok.Data;
import org.springframework.data.annotation.Id;

import javax.validation.constraints.NotNull;

@Data
public class Node {
    @Id
    private String id;
    @NotNull
    private String type;
    @NotNull
    private NodePosition position;
    private String sourcePosition;
    private String targetPosition;
    private Boolean isHidden;
    private NodeData data;
    private String desc;
    private String className;
}
