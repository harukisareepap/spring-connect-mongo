package com.example.springconnectmongo.dto.flow;

import lombok.Data;

@Data
public class NodePosition {
    private Double x;
    private Double y;
}
