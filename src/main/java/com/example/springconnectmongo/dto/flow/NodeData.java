package com.example.springconnectmongo.dto.flow;

import lombok.Data;

@Data
public class NodeData {
    private String name;
    private String icon;
}
