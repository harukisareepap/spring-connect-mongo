package com.example.springconnectmongo.service;

import com.example.springconnectmongo.dto.CustomerDto;
import com.example.springconnectmongo.repository.CustomerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class CustomerService {
    private CustomerRepository repository;

    @Autowired
    public CustomerService(CustomerRepository repository) {
        this.repository = repository;
    }

    public List<CustomerDto> retrieveCustomers() {
        return repository.findAll();
    }

    public Optional<CustomerDto> retrieveCustomers(String id) {
        return repository.findById(id);
    }

    public List<CustomerDto> retrieveCustomersByName(String name) {
        return repository.findByFirstName(name);
    }

    public CustomerDto createCustomer(CustomerDto customer) {
        return repository.save(customer);
    }

    public Optional<CustomerDto> updateCustomer(String id, CustomerDto customer) {
        Optional<CustomerDto> customerOpt = repository.findById(id);
        if (!customerOpt.isPresent()) {
            return customerOpt;
        }
        customer.setId(id);
        return Optional.of(repository.save(customer));
    }

    public boolean deleteCustomer(String id) {
        try {
            repository.deleteById(id);
            return true;
        } catch (Exception e) {
            return false;
        }
    }
}
