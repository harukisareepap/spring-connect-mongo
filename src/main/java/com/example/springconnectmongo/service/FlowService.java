package com.example.springconnectmongo.service;

import com.example.springconnectmongo.dto.flow.Flow;
import com.example.springconnectmongo.repository.FlowRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Slf4j
@Service
public class FlowService {

    @Autowired
    private FlowRepository flowRepository;

    public Flow create(Flow flow) {
        return flowRepository.save(flow);
    }

    public Optional<Flow> update(String id, Flow flow) {
        Optional<Flow> optionalFlow = flowRepository.findById(id);
        if (!optionalFlow.isPresent()) {
            return optionalFlow;
        }
        flow.setId(id);
        return Optional.of(flowRepository.save(flow));
    }

    public List<Flow> getAll() {
        return flowRepository.findAll();
    }

    public Optional<Flow> getById(String id) {
        return flowRepository.findById(id);
    }
}
