package com.example.springconnectmongo.service;

import com.example.springconnectmongo.entity.TblMdDataSourceEntity;
import com.example.springconnectmongo.repository.TblMdDataSourceRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class DataSourceService {

  private final TblMdDataSourceRepository tblMdDataSourceRepository;

  @Autowired
  public DataSourceService(TblMdDataSourceRepository tblMdDataSourceRepository) {
    this.tblMdDataSourceRepository = tblMdDataSourceRepository;
  }


  public List<TblMdDataSourceEntity> findAllDataSource(){
    return tblMdDataSourceRepository.findAll();
  }

  public void saveDataSource(TblMdDataSourceEntity entity){
    tblMdDataSourceRepository.save(entity);
  }
}
