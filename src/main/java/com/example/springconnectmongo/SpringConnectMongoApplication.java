package com.example.springconnectmongo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringConnectMongoApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringConnectMongoApplication.class, args);
	}

}
