package com.example.springconnectmongo.entity;

import javax.persistence.Id;
import javax.validation.constraints.NotNull;
import lombok.Data;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@Document(collection = "TBL_MD_DATA_SOURCE")
public class TblMdDataSourceEntity {

  @Id private String _id;
  @NotNull private Integer DATA_SOURCE_ID;
  @NotNull private String DATA_SOURCE_TYPE;
  @NotNull private String DATA_SOURCE_NAME;
}
