package com.example.springconnectmongo.repository;

import com.example.springconnectmongo.dto.CustomerDto;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CustomerRepository extends MongoRepository<CustomerDto, String> {
    List<CustomerDto> findByFirstName(String firstName);
}
