package com.example.springconnectmongo.repository;

import com.example.springconnectmongo.entity.TblMdDataSourceEntity;
import java.util.List;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TblMdDataSourceRepository extends MongoRepository<TblMdDataSourceEntity, String> {
//  List<TblMdDataSourceEntity> findByDataSourceType(String dataSourceType);
}
