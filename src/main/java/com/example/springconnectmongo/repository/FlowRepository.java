package com.example.springconnectmongo.repository;

import com.example.springconnectmongo.dto.flow.Flow;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FlowRepository extends MongoRepository<Flow, String> {
}
